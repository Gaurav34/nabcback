const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const usersModel = new Schema(
  {
    formType:String,
    prefix: String,
    firstName: String,
    lastName: String,
    gender: String,
    email: String,
    password: String,

    country: String,
    countryCode: String,

    phoneNumber: String,

    addressLine1: String,
    addressLine2: String,

    city: String,
    postalCode: String,
    state: String,
    fee: String,
    token: String,
    cardID: String,
    last4: String,
    emailPay: String,
    clientIp: String,
    price: String,
    currency:String,
    paymentId:String,
    client_secret:String,
    status:String,
    payment_method_id:String,
AffiliatedOrganization:String,
Organization:String
   
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("infousers", usersModel);

// import * as mongoose from "mongoose";

// export interface usersModelTypes extends mongoose.Document {
//   name: String;
//   age: Number;
//   email: String;
//   status: Boolean;
//   phoneNumber: Number;
// }

// export const usersModelSchema = new mongoose.Schema(
//   {
//     name: { type: String, required: true },
//     IpVoted: { type: Array }
//   },
//   {
//     timestamps: true
//   }
// );

// const userModel = mongoose.model<usersModelTypes>("surveys", usersModelSchema);
// export default userModel;
