const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const usersModel = require("./models/users");
const inforUser = require("./models/users");
const stripe = require("stripe")(
  "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"
);
app.use(express.static("."));
// pk_live_ZReBEjYfxIt8cnOwdiwrZUCJ00jxvz4T5i
// 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
const STRIPE_SDK = require("stripe");
const port = 3001;
const cors = require("cors");
mongoose.set("useFindAndModify", false);
var jsonParser = bodyParser.json();
//app.use(cors())
app.use(express.json());

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());
app.get("/", (req, res) => res.send("0Hello World!"));

// app.header('Content-Type', 'application/json');
// app.header('Access-Control-Allow-Origin', '*');
// app.header('Access-Control-Allow-Headers', 'Content-Type');

const uri =
"mongodb://test:128SgqEEQzxpCE8m@cluster0-shard-00-00.kw5yl.mongodb.net:27017,cluster0-shard-00-01.kw5yl.mongodb.net:27017,cluster0-shard-00-02.kw5yl.mongodb.net:27017/stripe?ssl=true&replicaSet=atlas-11cxv8-shard-0&authSource=admin&retryWrites=true&w=majority";
mongoose
  .connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    app.listen(port, () =>
      console.log(`Example app listening on ssssport ${port}!`)
    );
  })
  .catch((err) => console.log(err));

app.post("/sponsors", async (req, res) => {
  let emailtest = await usersModel.find({
    email: req.body.email,
  });

  if (emailtest.length > 0) {
    res.send({
      data: "alredyRegister",
    });
  } else {
    let newgame = new usersModel({
      formType: req.body.formType,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      gender: req.body.salutation,
      email: req.body.email,
      password: req.body.password,

      country: req.body.country,
      countryCode: req.body.countryCode,

      phoneNumber: req.body.phoneNumber,

      addressLine1: req.body.addressLine1,
      addressLine2: req.body.addressLine2,

      city: req.body.city,
      postalCode: req.body.postalCode,
      state: req.body.state,
      fee: req.body.price,

      currency: req.body.currency,
      price: req.body.price,
      AffiliatedOrganization: req.body.AffiliatedOrganization,

      paymentId: req.body.paymentId,
      client_secret: req.body.client_secret,
      status: req.body.status,
      payment_method_id: req.body.payment_method_id,
    });
    try {
      let response = await newgame.save();

      const mailgun = require("mailgun-js");
      const DOMAIN = "nabcglobal.virtuallive.in";
      const mg = mailgun({
        apiKey: "f5a25a552a275b5e7849d9c0c16c5abc-3d0809fb-6818b8b1",
        domain: DOMAIN,
      });
      const data = {
        to: req.body.email,
        from: "noreply@nabcglobal.virtuallive.in", // Change to your verified sender
        subject: "Welcome to NABC GLOBAL CONFERENCE ",

        html: `Hello,
         <br><br>
         Thank you for registering for NABC GLOBAL 2021 the enhanced, global & digital version of traditional NABC [ North American Bengali Conference or Bongo Sammelon] connecting Bengalis all over the world.

        
         <br><br> 
         Save this email to refer your login credentials if you have trouble logging in.
    
    <br><br>     <b>Login credentials:</b>
    <br> <b> First Name: ${req.body.firstName}  </b> 
    <br> <b> Last Name: ${req.body.lastName}  </b> 
      <br>   <br>  <b> Email ID:${req.body.email} </b>
        <br> <b> Password: ${req.body.password}  </b> 
        <br><br> <b>*Note: This is an auto generated email. Do not reply to this email. Do not delete / archive this mail till July 04, 2021
        Please do not forget to block your time in your calendar.
        </b>
        <br><br>
        Experience an immersive and interactive 3D Virtual Platform. The audience will attend conference in a customized digital environment with all traditional elements of NABC and many more.

    
        <br><br>
      <b>  POINTS To PONDER
      </b>
      <br/>

      •	Your log-in access and event link will work for one IP address hence one internet linked device like desk top, laptop, tablet or mobile.
      <br/>
      •	For navigation and interaction purpose use touch screen device or any cursor [ mouse, touch pen] based device. You can watch on TV by casting or connecting from your log in device to smart TV.
      <br/>•	Please do not share your log in credential with others and save this confirmation email.

      <br/>
      •	SHARE the excitement with your friends and family and ask everybody to join pool of biggest global Bengali audience on our Virtual Reality (VR) platform. In association with global partners.

      <br/>

      <br/>
      Thank You
      <br/>
      <br/>
      NABC GLOBAL TEAM 
& 
Milan Awon <br/>

Convener – NABC GLOBAL CONFERENCE 2021 <br/>
www.nabcglobal.org <br/>
<br/>


CONTACT FOR REGISTRATION INFORMATION <br/>
Jyotirmoy Sarkar : [Registration update ] <br/>
Phone : 727 643 3691 <br/>
Email : registration@nabcglobal.org       <br/>
<br/>

Ranadeb Sarkar [ For payment confirmation]  <br/>
Phone : 516 965 5277  <br/>
Email : ranusarkar@hotmail.com  <br/>

<br/>

Partha Chakraborty : [ Registration roll over from 2020 ] <br/>
Phone : 732 491 3316 <br/>
Email : registration@nabcglobal.org <br/>


`,
      };
      mg.messages().send(data, function (error, body) {
        console.log(body);
        if (body) {
          res.send({ data: "done", res: response, body });

          return response;
        }
      });
    } catch (error) {
      return error;
    }
  }
});

app.post("/standard", async (req, res) => {
  let emailtest = await usersModel.find({
    email: req.body.email,
  });

  if (emailtest.length > 0) {
    res.send({
      data: "alredyRegister",
    });
  } else {
    let newgame = new usersModel({
      formType: req.body.formType,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      gender: req.body.salutation,
      email: req.body.email,
      password: req.body.password,

      country: req.body.country,
      countryCode: req.body.countryCode,

      phoneNumber: req.body.phoneNumber,

      addressLine1: req.body.addressLine1,
      addressLine2: req.body.addressLine2,

      city: req.body.city,
      postalCode: req.body.postalCode,
      state: req.body.state,
      fee: req.body.price,

      currency: req.body.currency,
      price: req.body.price,
      AffiliatedOrganization: req.body.AffiliatedOrganization,

      paymentId: req.body.paymentId,
      client_secret: req.body.client_secret,
      status: req.body.status,
      payment_method_id: req.body.payment_method_id,
    });
    try {
      let response = await newgame.save();

      const mailgun = require("mailgun-js");
      const DOMAIN = "nabcglobal.virtuallive.in";
      const mg = mailgun({
        apiKey: "f5a25a552a275b5e7849d9c0c16c5abc-3d0809fb-6818b8b1",
        domain: DOMAIN,
      });
      const data = {
        to: req.body.email,
        from: "noreply@nabcglobal.virtuallive.in", // Change to your verified sender
        subject: "Welcome to NABC GLOBAL CONFERENCE ",

        html: `Hello,
         <br><br>
         Thank you for registering for NABC GLOBAL 2021 the enhanced, global & digital version of traditional NABC [ North American Bengali Conference or Bongo Sammelon] connecting Bengalis all over the world.

        
         <br><br> 
         Save this email to refer your login credentials if you have trouble logging in.
    
    <br><br>     <b>Login credentials:</b>
    <br> <b> First Name: ${req.body.firstName}  </b> 
    <br> <b> Last Name: ${req.body.lastName}  </b> 
      <br>   <br>  <b> Email ID:${req.body.email} </b>
        <br> <b> Password: ${req.body.password}  </b> 
        <br><br> <b>*Note: This is an auto generated email. Do not reply to this email. Do not delete / archive this mail till July 04, 2021
        Please do not forget to block your time in your calendar.
        </b>
        <br><br>
        Experience an immersive and interactive 3D Virtual Platform. The audience will attend conference in a customized digital environment with all traditional elements of NABC and many more.

    
        <br><br>
      <b>  POINTS To PONDER
      </b>
      <br/>

      •	Your log-in access and event link will work for one IP address hence one internet linked device like desk top, laptop, tablet or mobile.
      <br/>
      •	For navigation and interaction purpose use touch screen device or any cursor [ mouse, touch pen] based device. You can watch on TV by casting or connecting from your log in device to smart TV.
      <br/>•	Please do not share your log in credential with others and save this confirmation email.

      <br/>
      •	SHARE the excitement with your friends and family and ask everybody to join pool of biggest global Bengali audience on our Virtual Reality (VR) platform. In association with global partners.

      <br/>

      <br/>
      Thank You
      <br/>
      <br/>
      NABC GLOBAL TEAM 
& 
Milan Awon <br/>

Convener – NABC GLOBAL CONFERENCE 2021 <br/>
www.nabcglobal.org <br/>
<br/>


CONTACT FOR REGISTRATION INFORMATION <br/>
Jyotirmoy Sarkar : [Registration update ] <br/>
Phone : 727 643 3691 <br/>
Email : registration@nabcglobal.org       <br/>
<br/>

Ranadeb Sarkar [ For payment confirmation]  <br/>
Phone : 516 965 5277  <br/>
Email : ranusarkar@hotmail.com  <br/>

<br/>

Partha Chakraborty : [ Registration roll over from 2020 ] <br/>
Phone : 732 491 3316 <br/>
Email : registration@nabcglobal.org <br/>


`,
      };
      mg.messages().send(data, function (error, body) {
        console.log(body);
        if (body) {
          res.send({ data: "done", res: response, body });

          return response;
        }
      });
    } catch (error) {
      return error;
    }
  }
});





app.post("/registration", async (req, res) => {
  let emailtest = await usersModel.find({
    email: req.body.email,
  });

  if (emailtest.length > 0) {
    res.send({
      data: "alredyRegister",
    });
  } else {
    let newgame = new usersModel({
      formType: req.body.formType,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      gender: req.body.salutation,
      email: req.body.email,
      password: req.body.password,

      country: req.body.country,
      countryCode: req.body.countryCode,

      phoneNumber: req.body.phoneNumber,

      addressLine1: req.body.addressLine1,
      addressLine2: req.body.addressLine2,

      city: req.body.city,
      postalCode: req.body.postalCode,
      state: req.body.state,
      fee: req.body.price,

      currency: req.body.currency,
      price: req.body.price,
      AffiliatedOrganization: req.body.AffiliatedOrganization,

      paymentId: req.body.paymentId,
      client_secret: req.body.client_secret,
      status: req.body.status,
      payment_method_id: req.body.payment_method_id,
    });
    try {
      let response = await newgame.save();

      const mailgun = require("mailgun-js");
      const DOMAIN = "nabcglobal.virtuallive.in";
      const mg = mailgun({
        apiKey: "f5a25a552a275b5e7849d9c0c16c5abc-3d0809fb-6818b8b1",
        domain: DOMAIN,
      });
      const data = {
        to: req.body.email,
        from: "noreply@nabcglobal.virtuallive.in", // Change to your verified sender
        subject: "Welcome to NABC GLOBAL CONFERENCE ",

        html: `Hello,
         <br><br>
         Thank you for registering for NABC GLOBAL 2021 the enhanced, global & digital version of traditional NABC [ North American Bengali Conference or Bongo Sammelon] connecting Bengalis all over the world.

        
         <br><br> 
         Save this email to refer your login credentials if you have trouble logging in.
    
    <br><br>     <b>Login credentials:</b>
    <br> <b> First Name: ${req.body.firstName}  </b> 
    <br> <b> Last Name: ${req.body.lastName}  </b> 
      <br>   <br>  <b> Email ID:${req.body.email} </b>
        <br> <b> Password: ${req.body.password}  </b> 
        <br><br> <b>*Note: This is an auto generated email. Do not reply to this email. Do not delete / archive this mail till July 04, 2021
        Please do not forget to block your time in your calendar.
        </b>
        <br><br>
        Experience an immersive and interactive 3D Virtual Platform. The audience will attend conference in a customized digital environment with all traditional elements of NABC and many more.

    
        <br><br>
      <b>  POINTS To PONDER
      </b>
      <br/>

      •	Your log-in access and event link will work for one IP address hence one internet linked device like desk top, laptop, tablet or mobile.
      <br/>
      •	For navigation and interaction purpose use touch screen device or any cursor [ mouse, touch pen] based device. You can watch on TV by casting or connecting from your log in device to smart TV.
      <br/>•	Please do not share your log in credential with others and save this confirmation email.

      <br/>
      •	SHARE the excitement with your friends and family and ask everybody to join pool of biggest global Bengali audience on our Virtual Reality (VR) platform. In association with global partners.

      <br/>

      <br/>
      Thank You
      <br/>
      <br/>
      NABC GLOBAL TEAM 
& 
Milan Awon <br/>

Convener – NABC GLOBAL CONFERENCE 2021 <br/>
www.nabcglobal.org <br/>
<br/>


CONTACT FOR REGISTRATION INFORMATION <br/>
Jyotirmoy Sarkar : [Registration update ] <br/>
Phone : 727 643 3691 <br/>
Email : registration@nabcglobal.org       <br/>
<br/>

Ranadeb Sarkar [ For payment confirmation]  <br/>
Phone : 516 965 5277  <br/>
Email : ranusarkar@hotmail.com  <br/>

<br/>

Partha Chakraborty : [ Registration roll over from 2020 ] <br/>
Phone : 732 491 3316 <br/>
Email : registration@nabcglobal.org <br/>


`,
      };
      mg.messages().send(data, function (error, body) {
        console.log(body);
        if (body) {
          res.send({ data: "done", res: response, body });

          return response;
        }
      });
    } catch (error) {
      return error;
    }
  }
});

app.post("/create-payment-intent", async (req, res) => {
  console.log("items", req.body);

  const paymentIntent = await stripe.paymentIntents.create({
    amount: parseInt(req.body.money),
    currency: "usd",
    receipt_email: req.body.email,
  });
  if (paymentIntent.client_secret) {
    console.log("sucess", req.body);
    res.send({
      clientSecret: paymentIntent.client_secret,
    });
  }
});

app.post("/donor", async (req, res) => {

  let emailtest = await usersModel.find({
    email: req.body.email,
  });

  if (emailtest.length > 0) {
    res.send({
      data: "alredyRegister",
    });
  } else {
    let newgame = new usersModel({
      formType: req.body.formType,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      gender: req.body.salutation,
      email: req.body.email,
      password: req.body.password,

      country: req.body.country,
      countryCode: req.body.countryCode,

      phoneNumber: req.body.phoneNumber,

      addressLine1: req.body.addressLine1,
      addressLine2: req.body.addressLine2,

      city: req.body.city,
      postalCode: req.body.postalCode,
      state: req.body.state,
      fee: req.body.price,

      currency: req.body.currency,
      price: req.body.price,
      AffiliatedOrganization: req.body.AffiliatedOrganization,

      paymentId: req.body.paymentId,
      client_secret: req.body.client_secret,
      status: req.body.status,
      payment_method_id: req.body.payment_method_id,
    });
    try {
      let response = await newgame.save();

      const mailgun = require("mailgun-js");
      const DOMAIN = "nabcglobal.virtuallive.in";
      const mg = mailgun({
        apiKey: "f5a25a552a275b5e7849d9c0c16c5abc-3d0809fb-6818b8b1",
        domain: DOMAIN,
      });
      const data = {
        to: req.body.email,
        from: "noreply@nabcglobal.virtuallive.in", // Change to your verified sender
        subject: "Welcome to NABC GLOBAL CONFERENCE ",

        html: `Hello,
         <br><br>
         Thank you for registering for NABC GLOBAL 2021 the enhanced, global & digital version of traditional NABC [ North American Bengali Conference or Bongo Sammelon] connecting Bengalis all over the world.

        
         <br><br> 
         Save this email to refer your login credentials if you have trouble logging in.
    
    <br><br>     <b>Login credentials:</b>
    <br> <b> First Name: ${req.body.firstName}  </b> 
    <br> <b> Last Name: ${req.body.lastName}  </b> 
      <br>   <br>  <b> Email ID:${req.body.email} </b>
        <br> <b> Password: ${req.body.password}  </b> 
        <br><br> <b>*Note: This is an auto generated email. Do not reply to this email. Do not delete / archive this mail till July 04, 2021
        Please do not forget to block your time in your calendar.
        </b>
        <br><br>
        Experience an immersive and interactive 3D Virtual Platform. The audience will attend conference in a customized digital environment with all traditional elements of NABC and many more.

    
        <br><br>
      <b>  POINTS To PONDER
      </b>
      <br/>

      •	Your log-in access and event link will work for one IP address hence one internet linked device like desk top, laptop, tablet or mobile.
      <br/>
      •	For navigation and interaction purpose use touch screen device or any cursor [ mouse, touch pen] based device. You can watch on TV by casting or connecting from your log in device to smart TV.
      <br/>•	Please do not share your log in credential with others and save this confirmation email.

      <br/>
      •	SHARE the excitement with your friends and family and ask everybody to join pool of biggest global Bengali audience on our Virtual Reality (VR) platform. In association with global partners.

      <br/>

      <br/>
      Thank You
      <br/>
      <br/>
      NABC GLOBAL TEAM 
& 
Milan Awon <br/>

Convener – NABC GLOBAL CONFERENCE 2021 <br/>
www.nabcglobal.org <br/>
<br/>


CONTACT FOR REGISTRATION INFORMATION <br/>
Jyotirmoy Sarkar : [Registration update ] <br/>
Phone : 727 643 3691 <br/>
Email : registration@nabcglobal.org       <br/>
<br/>

Ranadeb Sarkar [ For payment confirmation]  <br/>
Phone : 516 965 5277  <br/>
Email : ranusarkar@hotmail.com  <br/>

<br/>

Partha Chakraborty : [ Registration roll over from 2020 ] <br/>
Phone : 732 491 3316 <br/>
Email : registration@nabcglobal.org <br/>


`,
      };
      mg.messages().send(data, function (error, body) {
        console.log(body);
        if (body) {
          res.send({ data: "done", res: response, body });

          return response;
        }
      });
    } catch (error) {
      return error;
    }
  }
});

app.post("/partner", async (req, res) => {
  let emailtest = await usersModel.find({
    email: req.body.email,
  });

  if (emailtest.length > 0) {
    res.send({
      data: "alredyRegister",
    });
  } else {
    let newgame = new usersModel({
      formType: req.body.formType,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      gender: req.body.salutation,
      email: req.body.email,
      password: req.body.password,

      country: req.body.country,
      countryCode: req.body.countryCode,

      phoneNumber: req.body.phoneNumber,

      addressLine1: req.body.addressLine1,
      addressLine2: req.body.addressLine2,

      city: req.body.city,
      postalCode: req.body.postalCode,
      state: req.body.state,
      fee: req.body.price,

      currency: req.body.currency,
      price: req.body.price,
      AffiliatedOrganization: req.body.AffiliatedOrganization,

      paymentId: req.body.paymentId,
      client_secret: req.body.client_secret,
      status: req.body.status,
      payment_method_id: req.body.payment_method_id,
    });
    try {
      let response = await newgame.save();

      const mailgun = require("mailgun-js");
      const DOMAIN = "nabcglobal.virtuallive.in";
      const mg = mailgun({
        apiKey: "f5a25a552a275b5e7849d9c0c16c5abc-3d0809fb-6818b8b1",
        domain: DOMAIN,
      });
      const data = {
        to: req.body.email,
        from: "noreply@nabcglobal.virtuallive.in", // Change to your verified sender
        subject: "Welcome to NABC GLOBAL CONFERENCE ",

        html: `Hello,
         <br><br>
         Thank you for registering for NABC GLOBAL 2021 the enhanced, global & digital version of traditional NABC [ North American Bengali Conference or Bongo Sammelon] connecting Bengalis all over the world.

        
         <br><br> 
         Save this email to refer your login credentials if you have trouble logging in.
    
    <br><br>     <b>Login credentials:</b>
    <br> <b> First Name: ${req.body.firstName}  </b> 
    <br> <b> Last Name: ${req.body.lastName}  </b> 
      <br>   <br>  <b> Email ID:${req.body.email} </b>
        <br> <b> Password: ${req.body.password}  </b> 
        <br><br> <b>*Note: This is an auto generated email. Do not reply to this email. Do not delete / archive this mail till July 04, 2021
        Please do not forget to block your time in your calendar.
        </b>
        <br><br>
        Experience an immersive and interactive 3D Virtual Platform. The audience will attend conference in a customized digital environment with all traditional elements of NABC and many more.

    
        <br><br>
      <b>  POINTS To PONDER
      </b>
      <br/>

      •	Your log-in access and event link will work for one IP address hence one internet linked device like desk top, laptop, tablet or mobile.
      <br/>
      •	For navigation and interaction purpose use touch screen device or any cursor [ mouse, touch pen] based device. You can watch on TV by casting or connecting from your log in device to smart TV.
      <br/>•	Please do not share your log in credential with others and save this confirmation email.

      <br/>
      •	SHARE the excitement with your friends and family and ask everybody to join pool of biggest global Bengali audience on our Virtual Reality (VR) platform. In association with global partners.

      <br/>

      <br/>
      Thank You
      <br/>
      <br/>
      NABC GLOBAL TEAM 
& 
Milan Awon <br/>

Convener – NABC GLOBAL CONFERENCE 2021 <br/>
www.nabcglobal.org <br/>
<br/>


CONTACT FOR REGISTRATION INFORMATION <br/>
Jyotirmoy Sarkar : [Registration update ] <br/>
Phone : 727 643 3691 <br/>
Email : registration@nabcglobal.org       <br/>
<br/>

Ranadeb Sarkar [ For payment confirmation]  <br/>
Phone : 516 965 5277  <br/>
Email : ranusarkar@hotmail.com  <br/>

<br/>

Partha Chakraborty : [ Registration roll over from 2020 ] <br/>
Phone : 732 491 3316 <br/>
Email : registration@nabcglobal.org <br/>


`,
      };
      mg.messages().send(data, function (error, body) {
        console.log(body);
        if (body) {
          res.send({ data: "done", res: response, body });

          return response;
        }
      });
    } catch (error) {
      return error;
    }
  }
});


app.post("/covid", async (req, res) => {
  let emailtest = await usersModel.find({
    email: req.body.email,
  });

  if (emailtest.length > 0) {
    res.send({
      data: "alredyRegister",
    });
  } else {
    let newgame = new usersModel({
      formType: req.body.formType,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      gender: req.body.salutation,
      email: req.body.email,
      password: req.body.password,

      country: req.body.country,
      countryCode: req.body.countryCode,

      phoneNumber: req.body.phoneNumber,

      addressLine1: req.body.addressLine1,
      addressLine2: req.body.addressLine2,

      city: req.body.city,
      postalCode: req.body.postalCode,
      state: req.body.state,
      fee: req.body.price,

      currency: req.body.currency,
      price: req.body.price,
      AffiliatedOrganization: req.body.AffiliatedOrganization,

      paymentId: req.body.paymentId,
      client_secret: req.body.client_secret,
      status: req.body.status,
      payment_method_id: req.body.payment_method_id,
    });
    try {
      let response = await newgame.save();

      const mailgun = require("mailgun-js");
      const DOMAIN = "nabcglobal.virtuallive.in";
      const mg = mailgun({
        apiKey: "f5a25a552a275b5e7849d9c0c16c5abc-3d0809fb-6818b8b1",
        domain: DOMAIN,
      });
      const data = {
        to: req.body.email,
        from: "noreply@nabcglobal.virtuallive.in", // Change to your verified sender
        subject: "Welcome to NABC GLOBAL CONFERENCE ",

        html: `Hello,
         <br><br>
         Thank you for registering for NABC GLOBAL 2021 the enhanced, global & digital version of traditional NABC [ North American Bengali Conference or Bongo Sammelon] connecting Bengalis all over the world.

        
         <br><br> 
         Save this email to refer your login credentials if you have trouble logging in.
    
    <br><br>     <b>Login credentials:</b>
    <br> <b> First Name: ${req.body.firstName}  </b> 
    <br> <b> Last Name: ${req.body.lastName}  </b> 
      <br>   <br>  <b> Email ID:${req.body.email} </b>
        <br> <b> Password: ${req.body.password}  </b> 
        <br><br> <b>*Note: This is an auto generated email. Do not reply to this email. Do not delete / archive this mail till July 04, 2021
        Please do not forget to block your time in your calendar.
        </b>
        <br><br>
        Experience an immersive and interactive 3D Virtual Platform. The audience will attend conference in a customized digital environment with all traditional elements of NABC and many more.

    
        <br><br>
      <b>  POINTS To PONDER
      </b>
      <br/>

      •	Your log-in access and event link will work for one IP address hence one internet linked device like desk top, laptop, tablet or mobile.
      <br/>
      •	For navigation and interaction purpose use touch screen device or any cursor [ mouse, touch pen] based device. You can watch on TV by casting or connecting from your log in device to smart TV.
      <br/>•	Please do not share your log in credential with others and save this confirmation email.

      <br/>
      •	SHARE the excitement with your friends and family and ask everybody to join pool of biggest global Bengali audience on our Virtual Reality (VR) platform. In association with global partners.

      <br/>

      <br/>
      Thank You
      <br/>
      <br/>
      NABC GLOBAL TEAM 
& 
Milan Awon <br/>

Convener – NABC GLOBAL CONFERENCE 2021 <br/>
www.nabcglobal.org <br/>
<br/>


CONTACT FOR REGISTRATION INFORMATION <br/>
Jyotirmoy Sarkar : [Registration update ] <br/>
Phone : 727 643 3691 <br/>
Email : registration@nabcglobal.org       <br/>
<br/>

Ranadeb Sarkar [ For payment confirmation]  <br/>
Phone : 516 965 5277  <br/>
Email : ranusarkar@hotmail.com  <br/>

<br/>

Partha Chakraborty : [ Registration roll over from 2020 ] <br/>
Phone : 732 491 3316 <br/>
Email : registration@nabcglobal.org <br/>


`,
      };
      mg.messages().send(data, function (error, body) {
        console.log(body);
        if (body) {
          res.send({ data: "done", res: response, body });

          return response;
        }
      });
    } catch (error) {
      return error;
    }
  }
});



app.post("/exhibitor", async (req, res) => {
  let emailtest = await usersModel.find({
    email: req.body.email,
  });

  if (emailtest.length > 0) {
    res.send({
      data: "alredyRegister",
    });
  } else {
    let newgame = new usersModel({
      formType: req.body.formType,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      gender: req.body.salutation,
      email: req.body.email,
      password: req.body.password,

      country: req.body.country,
      countryCode: req.body.countryCode,

      phoneNumber: req.body.phoneNumber,

      addressLine1: req.body.addressLine1,
      addressLine2: req.body.addressLine2,

      city: req.body.city,
      postalCode: req.body.postalCode,
      state: req.body.state,
      fee: req.body.price,

      currency: req.body.currency,
      price: req.body.price,
      AffiliatedOrganization: req.body.AffiliatedOrganization,

      paymentId: req.body.paymentId,
      client_secret: req.body.client_secret,
      status: req.body.status,
      payment_method_id: req.body.payment_method_id,
    });
    try {
      let response = await newgame.save();

      const mailgun = require("mailgun-js");
      const DOMAIN = "nabcglobal.virtuallive.in";
      const mg = mailgun({
        apiKey: "f5a25a552a275b5e7849d9c0c16c5abc-3d0809fb-6818b8b1",
        domain: DOMAIN,
      });
      const data = {
        to: req.body.email,
        from: "noreply@nabcglobal.virtuallive.in", // Change to your verified sender
        subject: "Welcome to NABC GLOBAL CONFERENCE ",

        html: `Hello,
         <br><br>
         Thank you for registering for NABC GLOBAL 2021 the enhanced, global & digital version of traditional NABC [ North American Bengali Conference or Bongo Sammelon] connecting Bengalis all over the world.

        
         <br><br> 
         Save this email to refer your login credentials if you have trouble logging in.
    
    <br><br>     <b>Login credentials:</b>
    <br> <b> First Name: ${req.body.firstName}  </b> 
    <br> <b> Last Name: ${req.body.lastName}  </b> 
      <br>   <br>  <b> Email ID:${req.body.email} </b>
        <br> <b> Password: ${req.body.password}  </b> 
        <br><br> <b>*Note: This is an auto generated email. Do not reply to this email. Do not delete / archive this mail till July 04, 2021
        Please do not forget to block your time in your calendar.
        </b>
        <br><br>
        Experience an immersive and interactive 3D Virtual Platform. The audience will attend conference in a customized digital environment with all traditional elements of NABC and many more.

    
        <br><br>
      <b>  POINTS To PONDER
      </b>
      <br/>

      •	Your log-in access and event link will work for one IP address hence one internet linked device like desk top, laptop, tablet or mobile.
      <br/>
      •	For navigation and interaction purpose use touch screen device or any cursor [ mouse, touch pen] based device. You can watch on TV by casting or connecting from your log in device to smart TV.
      <br/>•	Please do not share your log in credential with others and save this confirmation email.

      <br/>
      •	SHARE the excitement with your friends and family and ask everybody to join pool of biggest global Bengali audience on our Virtual Reality (VR) platform. In association with global partners.

      <br/>

      <br/>
      Thank You
      <br/>
      <br/>
      NABC GLOBAL TEAM 
& 
Milan Awon <br/>

Convener – NABC GLOBAL CONFERENCE 2021 <br/>
www.nabcglobal.org <br/>
<br/>


CONTACT FOR REGISTRATION INFORMATION <br/>
Jyotirmoy Sarkar : [Registration update ] <br/>
Phone : 727 643 3691 <br/>
Email : registration@nabcglobal.org       <br/>
<br/>

Ranadeb Sarkar [ For payment confirmation]  <br/>
Phone : 516 965 5277  <br/>
Email : ranusarkar@hotmail.com  <br/>

<br/>

Partha Chakraborty : [ Registration roll over from 2020 ] <br/>
Phone : 732 491 3316 <br/>
Email : registration@nabcglobal.org <br/>


`,
      };
      mg.messages().send(data, function (error, body) {
        console.log(body);
        if (body) {
          res.send({ data: "done", res: response, body });

          return response;
        }
      });
    } catch (error) {
      return error;
    }
  }
});

app.post("/findoneuser", async (req, res) => {
  console.log("sss", req.body);
  let emailtest = await  inforUser.find({
    email: req.body.email,
    
  });

  if (emailtest.length > 0) {
    let loginData = await inforUser.find({
      email: req.body.email,
      password: req.body.password,
    });

    if (loginData.length > 0) {
      res.send({
        data: loginData,
      });
    } else {
      res.send({
        data: "password is wrong",
      });
    }
  } else {
    res.send({
      data: "email address not found",
    });
  }
});

app.get("/alluser", async (req, res) => {
  let docs = await inforUser
  
  .find({});
console.log(docs)
  

  res.send({
count:docs.length,      data:docs});
});